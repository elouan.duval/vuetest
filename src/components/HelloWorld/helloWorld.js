const defineProps = {
  msg: {
    type: String,
    required: true,
  },
};
export default defineProps;
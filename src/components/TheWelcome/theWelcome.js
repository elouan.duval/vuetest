import WelcomeItem from '../WelcomeItem/WelcomeItem.vue'
import DocumentationIcon from '../icons/IconDocumentation.vue'
import ToolingIcon from '../icons/IconTooling.vue'
import EcosystemIcon from '../icons/IconEcosystem.vue'
import CommunityIcon from '../icons/IconCommunity.vue'
import SupportIcon from '../icons/IconSupport.vue'
import axios from 'axios';
import parseXml from 'xml-parser';
import md5 from 'md5';
import sha1 from 'sha1';

const API_URL_INIT = "http://apiv2.fftt.com/mobile/pxml/";
const SERIE = "";
// serie=WX1E7ZKJF23IBC8&tm=20230111095034082&tmc=d2d15fb0121ef00c665422d0eecae39b&id=SW724
const API_URL = 'http://apiv2.fftt.com/mobile/pxml/';
const CODE_APPLI = "SW724";
const TIMESTAMP_CRYPTED =md5("SEPmz69v5J");
var timestamp = sha1(Date.now());
var numLicence = 3527861;
var tm = 20230111095034082;

export default{
    name:'TheWelcome',
    components:{
        WelcomeItem,
        DocumentationIcon,
        ToolingIcon,
        EcosystemIcon,
        CommunityIcon,
        SupportIcon
    },
    data(){
        return{
            player : []
        }
    },
    methods : {
        async initialisation(){
            const response = await axios.get('${API_URL_INIT}xml_initialisation.php?serie=${SERIE}&tm=${tm}&tmc=${TIMESTAMP_CRYPTED}&id=${CODE_APPLI}',{
                headers: {
                    'Content-Type':'application/xml'
            },
        });
        return response;
        },
        async getPlayer() {
        try {
      const response = await axios.get(`${API_URL}xml_joueur.php?tm=${tm}&tmc=${TIMESTAMP_CRYPTED}&id=${CODE_APPLI}&licence=${numLicence}`, {
        headers: {
          'Content-Type': 'application/xml'
        },
      });
      const xml = response.data;
      const result = parseXml(xml);
      return result.root.children.reduce((acc, child) => {
        acc[child.name] = child.content;
        this.player = acc;
        console.log(acc);
        return acc;
      }, {});
    } catch (error) {
      console.error(error);
      return null;
    }
  }
    },
    created() {
        this.getPlayer()
    }
}
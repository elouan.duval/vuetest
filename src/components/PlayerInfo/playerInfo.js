import axios from 'axios';
import parseXml from 'xml-parser';
import md5 from 'md5';
import sha1 from 'sha1';

const API_URL = 'http://www.fftt.com/mobile/pxml/';
const CODE_APPLI = "SW724";
const TIMESTAMP_CRYPTED =md5("SEPmz69v5J");
var timestamp = sha1(Date.now());
var numLicence = 3527861;
//fonctionne : https://www.fftt.com/mobile/pxml/xml_joueur.php?tm=20230109095034082&tmc=d2d15fb0121ef00c665422d0eecae39b&id=SW724&licence=3527861


export default {
    data() {
        return { count: 0 }
      },
  async getPlayer() {
    try {
      const response = await axios.get(`https://www.fftt.com/mobile/pxml/xml_joueur.php?tm=20230111095034082&tmc=d2d15fb0121ef00c665422d0eecae39b&id=SW724&licence=3527861`, {
        headers: {
          'Content-Type': 'application/xml',
        },
      });
      const xml = response.data;
      const result = parseXml(xml);
      return result.root.children.reduce((acc, child) => {
        acc[child.name] = child.content;
        return acc;
      }, {});
    } catch (error) {
      console.error(error);
      return null;
    }
  }/*,
  getContacts: function(){
    axios.get(`${API_URL}xml_joueur.php?tm=${timestamp}&tmc=${TIMESTAMP_CRYPTED}&id=${CODE_APPLI}&licence=${numLicence}`)
    .then(function (response) {
        console.log(response.data);
    })
    .catch(function (error) {
        console.log(error);
    });
}*/
};

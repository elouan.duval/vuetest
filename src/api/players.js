export default {
    index( params ){
        return axios.get( 'https://music.com/api/v1/songs', {
            params: params
        })
    },

    show( id ){

    },

    update( id, data ){
        
    },

    create( data ){

    },

    delete( id ){

    }
}
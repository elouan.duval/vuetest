import HelloWorld from '../components/HelloWorld/HelloWorld.vue'
import TheWelcome from '../components/TheWelcome/TheWelcome.vue'

export default{
    name:'App',
    components:{
        HelloWorld,
        TheWelcome
    }
}
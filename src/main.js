import { createApp } from 'vue'
//import vueAxiosCors from 'vue-axios-cors'
import App from './App/App.vue'
import './assets/main.css'
//Vue.use(vueAxiosCors)
createApp(App).mount('#app')
